;;;; 2007-01-15 10:22:27

(defpackage #:trivial-freeimage-asd
  (:use :cl :cffi :asdf))

(in-package :trivial-freeimage-asd)

(defsystem trivial-freeimage
  :name "trivial-freeimage"
  :version "0.1"
  :components ((:file "defpackage")
               (:file "trivial-freeimage" :depends-on ("defpackage")))
  :depends-on ())
