
(in-package :trivial-freeimage)

(define-foreign-library freeimage
        (:unix "libfreeimage.so.3")
        (:windows "FreeImage.dll")
        (t (:default "libfreeimage")))
(use-foreign-library freeimage)

(defconstant +UNKNOWN+ -1)
(defconstant +BMP+ 0)
(defconstant +ICO+ 1)
(defconstant +JPEG+ 2)
(defconstant +JNG+ 3)
(defconstant +KOALA+ 4)
(defconstant +LBM+ 5)
(defconstant +IFF+ 5)
(defconstant +MNG+ 6)
(defconstant +PBM+ 7)
(defconstant +PBMRAW+	8)
(defconstant +PCD+ 9)
(defconstant +PCX+ 10)
(defconstant +PGM+ 11)
(defconstant +PGMRAW+ 12)
(defconstant +PNG+ 13)
(defconstant +PPM+ 14)
(defconstant +PPMRAW+ 15)
(defconstant +RAS+ 16)
(defconstant +TARGA+ 17)
(defconstant +TIFF+ 18)
(defconstant +WBMP+ 19)
(defconstant +PSD+ 20)
(defconstant +CUT+ 21)
(defconstant +XBM+ 22)
(defconstant +XPM+ 23)
(defconstant +DDS+ 24)
(defconstant +GIF+ 25)
(defconstant +HDR+ 26)
(defconstant +FAXG3+ 27)
(defconstant +SGI+ 28)


(defconstant +BMP_DEFAULT+ 0)
(defconstant +BMP_SAVE_RLE+ 1)
(defconstant +CUT_DEFAULT+ 0)
(defconstant +DDS_DEFAULT+ 0)
(defconstant +FAXG3_DEFAULT+ 0)
(defconstant +GIF_DEFAULT+ 0)
(defconstant +GIF_LOAD256+ 1)		  ;Load the image as a 256 color image with ununsed palette entries, if it's 16 or 2 color
(defconstant +GIF_PLAYBACK+	2)		;'Play' the GIF to generate each frame (as 32bpp) instead of returning raw frame data when loading
(defconstant +HDR_DEFAULT+ 0)
(defconstant +ICO_DEFAULT+ 0)
(defconstant +ICO_MAKEALPHA+ 1)		;convert to 32bpp and create an alpha channel from the AND-mask when loading
(defconstant +IFF_DEFAULT+ 0)
(defconstant +JPEG_DEFAULT+ 0)
(defconstant +JPEG_FAST+ 1)
(defconstant +JPEG_ACCURATE+ 2)
(defconstant +JPEG_QUALITYSUPERB+ #x80)
(defconstant +JPEG_QUALITYGOOD+ #x100)
(defconstant +JPEG_QUALITYNORMAL+ #x200)
(defconstant +JPEG_QUALITYAVERAGE+ #x400)
(defconstant +JPEG_QUALITYBAD+ #x800)
(defconstant +JPEG_CMYK+ #x1000)					;load separated CMYK "as is" (use | to combine with other flags)
(defconstant +JPEG_PROGRESSIVE+	#x2000)	;save as a progressive-JPEG (use | to combine with other flags)
(defconstant +KOALA_DEFAULT+ 0)
(defconstant +LBM_DEFAULT+ 0)
(defconstant +MNG_DEFAULT+ 0)
(defconstant +PCD_DEFAULT+ 0)
(defconstant +PCD_BASE+ 1)							;load the bitmap sized 768 x 512
(defconstant +PCD_BASEDIV4+ 2)					;load the bitmap sized 384 x 256
(defconstant +PCD_BASEDIV16+ 3)					;load the bitmap sized 192 x 128
(defconstant +PCX_DEFAULT+ 0)
(defconstant +PNG_DEFAULT+ 0)
(defconstant +PNG_IGNOREGAMMA+ 1)				;avoid gamma correction
(defconstant +PNM_DEFAULT+ 0)
(defconstant +PNM_SAVE_RAW+ 0)       		;If set the writer saves in RAW format (i.e. P4, P5 or P6)
(defconstant +PNM_SAVE_ASCII+ 1)        ;If set the writer saves in ASCII format (i.e. P1, P2 or P3)
(defconstant +PSD_DEFAULT+ 0)
(defconstant +RAS_DEFAULT+ 0)
(defconstant +SGI_DEFAULT+ 0)
(defconstant +TARGA_DEFAULT+ 0)
(defconstant +TARGA_LOAD_RGB888+ 1)     ;If set the loader converts RGB555 and ARGB8888 -> RGB888.
(defconstant +TIFF_DEFAULT+ 0)
(defconstant +TIFF_CMYK+ #x0001)					;reads/stores tags for separated CMYK (use | to combine with compression flags)
(defconstant +TIFF_PACKBITS+ #x0100)     ;save using PACKBITS compression
(defconstant +TIFF_DEFLATE+ #x0200)      ;save using DEFLATE compression (a.k.a. ZLIB compression)
(defconstant +TIFF_ADOBE_DEFLATE+ #x0400)  ;save using ADOBE DEFLATE compression
(defconstant +TIFF_NONE+ #x0800)					;save without any compression
(defconstant +TIFF_CCITTFAX3+	#x1000)    ;save using CCITT Group 3 fax encoding
(defconstant +TIFF_CCITTFAX4+	#x2000)    ;save using CCITT Group 4 fax encoding
(defconstant +TIFF_LZW+	#x4000)	        ;save using LZW compression
(defconstant +TIFF_JPEG+ #x8000)	        ;save using JPEG compression
(defconstant +WBMP_DEFAULT+ 0)
(defconstant +XBM_DEFAULT+ 0)
(defconstant +XPM_DEFAULT+ 0)

(defcstruct bitmapinfoheader
	(bisize		:unsigned-long)
	(biwidth	:long)
	(biheight	:long)
	(biplanes	:unsigned-short)
	(bibitcount	:unsigned-short)
	(bicompression	:unsigned-long)
	(bisizeimage	:unsigned-long)
	(bixpels	:long)
	(biypels	:long)
	(biclrused	:unsigned-long)
	(biclrimportant :unsigned-long))
(defcstruct rgbquad
	(rgbred :char)
	(rgbgreen :char)
	(rgbblue :char))
(defcstruct bitmap
	(bmiheader bitmapinfoheader)
	(bmicolors :pointer))

(defcfun ("FreeImage_Initialise" initialize) :void 
	(plugin :boolean))      

(defcfun ("FreeImage_DeInitialise" de-initialize) :void)

(defcfun ("FreeImage_GetVersion" get-version) :string)

(defcfun ("FreeImage_GetCopyrightMessage" get-copyright-message) :string)  



(defcfun ("FreeImage_Load" load-image) :pointer 
	(fif :int)
	(filename :string)
	(flags :int))

	 		(setf *current-dib* (freeimage-load (get-file-type file-name 0) file-name flags))
	 		
(defcfun ("FreeImage_Unload" unload-image) :void
	(dib :pointer))


(defcfun ("FreeImage_Save" save-image) :int
	(fif :int)
	(dib :pointer)
	(filename :string)
	(flags :int))

	
(defcfun ("FreeImage_Clone" clone-image) :pointer
	(dib :pointer))	

(defcfun ("FreeImage_GetImageType" get-image-type) :int
	(dib :pointer))

(defcfun ("FreeImage_GetInfoHeader" get-info-header) :pointer
  (dib :pointer))

(defcfun ("FreeImage_GetInfo" get-info) :pointer
  (dib :pointer))

(defcfun ("FreeImage_GetColorsUsed" get-colors-used) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetBPP" get-bpp) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetHeight" get-height) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetWidth" get-width) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetPitch" get-pitch) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetLine" get-line) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetDIBSize" get-dib-size) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetDotsPerMeterX" get-dots-per-meter-x) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetDotsPerMeterY" get-dots-per-meter-y) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_SetDotsPerMeterX" set-dots-per-meter-x) :void
	(dib :pointer)
	(res :unsigned-int))

(defcfun ("FreeImage_SetDotsPerMeterY" set-dots-per-meter-y) :void
	(dib :pointer)
	(res :unsigned-int))

(defcfun ("FreeImage_GetRedMask" get-red-mask) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetBlueMask" get-blue-mask) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetGreenMask" get-green-mask) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetTransparencyCount" get-transparency-count) :unsigned-int
	(dib :pointer))

(defcfun ("FreeImage_GetTransparencyTable" get-transparency-table) :pointer
	(dib :pointer))

(defcfun ("FreeImage_SetTransparencyTable" set-transparency-table) :void
	(dib :pointer)
	(table :pointer)
	(count :int))

(defcfun ("FreeImage_SetTransparent" set-transparent) :void
	(dib :pointer)
	(enabled :boolean))

(defcfun ("FreeImage_IsTransparent" is-transparent) :boolean
	(dib :pointer))

(defcfun ("FreeImage_HasBackgroundColor" has-background-color) :boolean
  (dib :pointer))

(defcfun ("FreeImage_GetBackgroundColor" get-background-color) :boolean
  (dib :pointer)
  (bkcolor :pointer))

(defcfun ("FreeImage_SetBackgroundColor" set-background-color) :boolean
  (dib :pointer)
  (bkcolor :pointer))

(defcfun ("FreeImage_GetFileType" get-file-type) :int
  (filename :string)
  (size :int))

(defcfun ("FreeImage_GetBits" get-bits) :pointer
  (dib :pointer))

(defcfun ("FreeImage_GetScanLine" get-scan-line) :pointer
  (dib :pointer)
  (scanline :int))

(defcfun ("FreeImage_GetPixelIndex" get-pixel-index) :boolean
  (dib :pointer)
  (x :unsigned-int)
  (y :unsigned-int)
  (value :pointer))

(defcfun ("FreeImage_GetPixelColor" get-pixel-color) :boolean
  (dib :pointer)
  (x :unsigned-int)
  (y :unsigned-int)
  (value :pointer))

(defcfun ("FreeImage_SetPixelIndex" set-pixel-index) :boolean
  (dib :pointer)
  (x :unsigned-int)
  (y :unsigned-int)
  (value :pointer))

(defcfun ("FreeImage_SetPixelColor" set-pixel-color) :boolean
  (dib :pointer)
  (x :unsigned-int)
  (y :unsigned-int)
  (value :pointer))


(defcfun ("FreeImage_ConvertTo4Bits" convert-to-4-bits) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ConvertTo8Bits" convert-to-8-bits) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ConvertToGreyscale" convert-to-greyscale) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ConvertTo16Bits555" convert-to-16-bits-555) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ConvertTo16Bits565" convert-to-16-bits-565) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ConvertTo24Bits" convert-to-24-bits) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ConvertTo32Bits" convert-to-32-bits) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ColorQuantize" color-quantize) :pointer
  (dib :pointer)
  (quantize :int))

(defcfun ("FreeImage_ColorQuantizeEx" color-quantize-ex) :pointer
  (dib :pointer)
  (quantize :int)
  (PaletteSize :int)
  (ReserveSize :int)
  (ReservePalette :pointer))

(defcfun ("FreeImage_Threshold" threshold) :pointer
  (dib :pointer)
  (t_arg1 :unsigned-char))

(defcfun ("FreeImage_Dither" dither) :pointer
  (dib :pointer)
  (algorithm :int))

(defcfun ("FreeImage_ConvertFromRawBits" convert-from-raw-bits) :pointer
  (bits :pointer)
  (width :int)
  (height :int)
  (pitch :int)
  (bpp :unsigned-int)
  (red_mask :unsigned-int)
  (green_mask :unsigned-int)
  (blue_mask :unsigned-int)
  (topdown :boolean))

(defcfun ("FreeImage_ConvertToRawBits" convert-to-raw-bits) :void
  (bits :pointer)
  (dib :pointer)
  (pitch :int)
  (bpp :unsigned-int)
  (red_mask :unsigned-int)
  (green_mask :unsigned-int)
  (blue_mask :unsigned-int)
  (topdown :boolean))

(defcfun ("FreeImage_ConvertToRGBF" convert-to-rgbf) :pointer
  (dib :pointer))

(defcfun ("FreeImage_ConvertToStandardType" convert-to-standard-type) :pointer
  (src :pointer)
  (scale_linear :boolean))

(defcfun ("FreeImage_ConvertToType" convert-to-type) :pointer
  (src :pointer)
  (dst_type :int)
  (scale_linear :boolean))

(defcfun ("FreeImage_RotateClassic" rotate-classic) :pointer
  (dib :pointer)
  (angle :double))

(defcfun ("FreeImage_RotateEx" rotate-ex) :pointer
  (dib :pointer)
  (angle :double)
  (x_shift :double)
  (y_shift :double)
  (x_origin :double)
  (y_origin :double)
  (use_mask :boolean))

(defcfun ("FreeImage_FlipHorizontal" flip-horizontal) :boolean
  (dib :pointer))

(defcfun ("FreeImage_FlipVertical" flip-vertical) :boolean
  (dib :pointer))

(defcfun ("FreeImage_JPEGTransform" jpeg-transform) :boolean
  (src_file :string)
  (dst_file :string)
  (operation :int)
  (perfect :boolean))

(defcfun ("FreeImage_Rescale" rescale) :pointer
  (dib :pointer)
  (dst_width :int)
  (dst_height :int)
  (filter :int))

(defcfun ("FreeImage_MakeThumbnail" make-thumbnail) :pointer
  (dib :pointer)
  (max_pixel_size :int)
  (convert :boolean))

(defcfun ("FreeImage_AdjustCurve" adjust-curve) :boolean
  (dib :pointer)
  (LUT :pointer)
  (channel :int))

(defcfun ("FreeImage_AdjustGamma" adjust-gamma) :boolean
  (dib :pointer)
  (gamma :double))

(defcfun ("FreeImage_AdjustBrightness" adjust-brightness) :boolean
  (dib :pointer)
  (percentage :double))

(defcfun ("FreeImage_AdjustContrast" adjust-contrast) :boolean
  (dib :pointer)
  (percentage :double))

(defcfun ("FreeImage_Invert" invert) :boolean
  (dib :pointer))

(defcfun ("FreeImage_GetHistogram" get-histogram) :boolean
  (dib :pointer)
  (histo :pointer)
  (channel :int))

(defcfun ("FreeImage_GetChannel" get-channel) :pointer
  (dib :pointer)
  (channel :int))

(defcfun ("FreeImage_SetChannel" set-channel) :boolean
  (dib :pointer)
  (dib8 :pointer)
  (channel :int))

(defcfun ("FreeImage_GetComplexChannel" get-complex-channel) :pointer
  (src :pointer)
  (channel :int))

(defcfun ("FreeImage_SetComplexChannel" set-complex-channel) :boolean
  (dst :pointer)
  (src :pointer)
  (channel :int))

(defcfun ("FreeImage_Copy" copy) :pointer
  (dib :pointer)
  (left :int)
  (top :int)
  (right :int)
  (bottom :int))

(defcfun ("FreeImage_Paste" paste) :boolean
  (dst :pointer)
  (src :pointer)
  (left :int)
  (top :int)
  (alpha :int))

(defcfun ("FreeImage_Composite" composite) :pointer
  (fg :pointer)
  (useFileBkg :boolean)
  (appBkColor :pointer)
  (bg :pointer))

(defcfun ("FreeImage_JPEGCrop" jpeg-crop) :boolean
  (src_file :string)
  (dst_file :string)
  (left :int)
  (top :int)
  (right :int)
  (bottom :int))

(defmacro with-dib ((dib-var file-name 
											&key (file-type-arg 0) (load-image-arg 0))
                      &body body)
    `(let ((,dib-var nil))
      (unwind-protect
           (progn
             (setf ,dib-var (load-image (get-file-type ,file-name ,file-type-arg) ,file-name  
                                        ,load-image-arg))
             ,@body)
        (when ,dib-var
          (unload-image ,dib-var)))))
     